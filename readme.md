# React project template
Simple react project template with recomended folders structure

**Based on:** vite, tailwindcss
**Includes:** eslint, prettier configs

**Recommended project structure:**
===========

- project
    - public 
    - src
        - assets
            - css
            - font
            - img
            - icons
        - components
            - shared
            - unique
        - hooks
        - pages 
        - routes
            - public.routes.js
            - private.routes.js
            - index.js
        App.jsx
        main.jsx

===========

# Using manual
For using template for your project as base platform, please follow next steps:
1. Fork repository to your own account
2. Clone forked repository to local machine 
3. Done! You can start your project based on this template.
